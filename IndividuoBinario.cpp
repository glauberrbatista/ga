/* 
 * File:   IndividuoBinario.cpp
 * Author: glauber
 * 
 * Created on 20 de Abril de 2016, 20:36
 */

#include "IndividuoBinario.h"

IndividuoBinario::IndividuoBinario() {
    this->setFitness(0.0);
    this->setObjetivo(0.0);
}

IndividuoBinario::IndividuoBinario(list<variavel*> *variaveis) {
    setVariaveis(variaveis);
    setQtdGenes(variaveis->size());
    gerarIndividuo();
}

IndividuoBinario::IndividuoBinario(const IndividuoBinario& orig) {
    *this = orig;
}

IndividuoBinario::~IndividuoBinario() {

}

void IndividuoBinario::setCromossomo(string cromossomo) {
    this->cromossomo = cromossomo;
}

string IndividuoBinario::getCromossomo() {
    return cromossomo;
}

void IndividuoBinario::setTamCromossomo(int tamCromossomo) {
    this->tamCromossomo = tamCromossomo;
}

int IndividuoBinario::getTamCromossomo() {
    return tamCromossomo;
}

void IndividuoBinario::gerarIndividuo() {
    int tamCromo = 0;
    for (variavel *v : * this->getVariaveis()) {
        tamCromo += ceil(log2((v->max - v->min) / v->precisao));
    }
    setCromossomo(stringBinaria(tamCromo));
    setTamCromossomo(tamCromo);
    setObjetivo(0.0);
    setFitness(0.0);
}

void IndividuoBinario::mutacao() {
    static mt19937 gerador(time(NULL));
    uniform_real_distribution<double> dis_double(0, 100);
    double ran;
    cromossomo = getCromossomo();

    for (int pos; pos < cromossomo.length(); pos++) {
        ran = dis_double(gerador);
        if (ran <= getChanceMutacao()) {
            if (cromossomo[pos] == '0') {
                cromossomo[pos] = '1';
            } else {
                cromossomo[pos] = '0';
            }
        }
    }

    setCromossomo(cromossomo);
}

void IndividuoBinario::calcFitness(pair<double, double> (*funcaoFitness)(void *)) {
    //normalização
    int inicioCorte = 0;
    int fimCorte = 0;
    list<pair<double, string>> genes;
    pair<double, string> gene;

    for (variavel *v : * this->getVariaveis()) {
        fimCorte = ceil(log2((v->max - v->min) / v->precisao)); // pega o numero de bits
        gene.second = this->cromossomo.substr(inicioCorte, fimCorte); //pega o gene
        gene.first = (int) (v->min + ((v->max - v->min) / (pow(2, fimCorte) - 1)) * stoi(gene.second, nullptr, 2)); //valor decimal normalizado
        genes.insert(genes.end(), gene);
        inicioCorte += fimCorte;
    }

    pair<double, double> resultado = funcaoFitness(&genes);
    setObjetivo(resultado.first);
    this->setFitness(max(0.0, resultado.second));
    //cout << getObjetivo() << " - " << getFitness() << endl;
}

pair<IndividuoBinario*, IndividuoBinario*> IndividuoBinario::crossover(IndividuoBinario *i, string tipo) {
    if (tipo == "umcorte") {
        return crossoverUmPonto(i);
    } else if (tipo == "uniforme") {
        return crossoverUniforme(i);
    } else {
        cout << "- Tipo de Crossover Inválido" << endl;
        exit(1);
    }
}

pair<IndividuoBinario*, IndividuoBinario*> IndividuoBinario::crossoverUmPonto(IndividuoBinario* i) {
    pair<IndividuoBinario*, IndividuoBinario*> resultado;
    resultado.first = new IndividuoBinario();
    resultado.second = new IndividuoBinario();
    *resultado.first = *this;
    *resultado.second = *i;

    static mt19937 gerador(time(NULL));
    uniform_int_distribution<int> dis(1, getTamCromossomo() - 1);
    uniform_real_distribution<double> dis_double(0, 100);
    string cromo1, cromo2;
    int corte;

    if (dis_double(gerador) <= getChanceCrossover()) {
        corte = dis(gerador);
        cromo1 = this->getCromossomo();
        cromo2 = i->getCromossomo();

        resultado.first->setCromossomo(cromo1.substr(0, corte) + cromo2.substr(corte, cromo2.length()));
        resultado.second->setCromossomo(cromo2.substr(0, corte) + cromo1.substr(corte, cromo1.length()));
    }

    return resultado;
}

pair<IndividuoBinario*, IndividuoBinario*> IndividuoBinario::crossoverUniforme(IndividuoBinario* i) {
    static mt19937 gerador(time(NULL));
    uniform_real_distribution<double> dis_double(0, 100);
    pair<IndividuoBinario*, IndividuoBinario*> resultado;
    resultado.first = new IndividuoBinario();
    resultado.second = new IndividuoBinario();
    *resultado.first = *this;
    *resultado.second = *i;
    string cromo1, cromo2;
    string mascara = stringBinaria(getTamCromossomo());
    char troca;

    cromo1 = this->getCromossomo();
    cromo2 = i->getCromossomo();
    if (dis_double(gerador) <= getChanceCrossover()) {
        for (int i = 0; i < mascara.length(); i++) {
            if (mascara[i] == '1') {
                troca = cromo1.at(i);
                cromo1.at(i) = cromo2.at(i);
                cromo2.at(i) = troca;
            }
        }
        resultado.first->setCromossomo(cromo1);
        resultado.second->setCromossomo(cromo2);
    }

    return resultado;
}

void IndividuoBinario::mostrarIndividuo() {
    cout << getCromossomo() << endl;
}

double IndividuoBinario::distancia(IndividuoBinario* b) {
    int temp = 0;
    for (int k = 0; k < getTamCromossomo(); k++) {
        if (this->getCromossomo().at(k) != b->getCromossomo().at(k)) {
            temp++;
        }
    }

    return temp;
}