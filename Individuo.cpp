/* 
 * File:   Individuo.cpp
 * Author: glauber
 * 
 * Created on 20 de Abril de 2016, 19:46
 */

#include <list>

#include "Individuo.h"

Individuo::Individuo() {
}

void Individuo::setTipoMutacao(string tipoMutacao) {
    this->tipoMutacao = tipoMutacao;
}

string Individuo::getTipoMutacao() {
    return tipoMutacao;
}

Individuo::Individuo(list<variavel*> *variaveis){
    setVariaveis(variaveis);
    setQtdGenes(variaveis->size());
}

Individuo::Individuo(const Individuo& orig) {
    *this = orig;
}

Individuo::~Individuo() {
}

void Individuo::setVariaveis(list<variavel*>* variaveis) {
    this->variaveis = variaveis;
}

list<variavel*>* Individuo::getVariaveis() {
    return variaveis;
}

void Individuo::setObjetivo(double objetivo) {
    this->objetivo = objetivo;
}

double Individuo::getObjetivo() {
    return objetivo;
}

void Individuo::setFitness(double fitness) {
    this->fitness = fitness;
}

double Individuo::getFitness() {
    return fitness;
}

void Individuo::setQtdGenes(int qtdGenes) {
    this->qtdGenes = qtdGenes;
}

int Individuo::getQtdGenes() {
    return qtdGenes;
}

void Individuo::setChanceMutacao(double chanceMutacao) {
    this->chanceMutacao = chanceMutacao;
}

double Individuo::getChanceMutacao() {
    return chanceMutacao;
}

void Individuo::setChanceCrossover(double chanceCrossover) {
    this->chanceCrossover = chanceCrossover;
}

double Individuo::getChanceCrossover() {
    return chanceCrossover;
}

string Individuo::stringBinaria(int tamanho) {
    string strBinaria;
    static mt19937 gerador(time(NULL));
    uniform_int_distribution<int> dis(0, 1);

    for (int i = 0; i < tamanho; i++) {
        strBinaria += to_string(dis(gerador));
    }

    return strBinaria;
}