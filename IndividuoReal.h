/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   IndividuoReal.h
 * Author: glauber
 *
 * Created on 22 de Abril de 2016, 14:49
 */

#ifndef INDIVIDUOREAL_H
#define INDIVIDUOREAL_H

#include <vector>
#include "Individuo.h"

class IndividuoReal : public Individuo{
public:
    IndividuoReal();
    IndividuoReal(list<variavel*> *variaveis);
    IndividuoReal(const IndividuoReal& orig);
    virtual ~IndividuoReal();
    void setCromossomo(vector<double>* cromossomo);
    vector<double>* getCromossomo();
    void mostrarIndividuo();
    
    void gerarIndividuo() override;
    void mutacao() override;
    void calcFitness(pair<double,double> (*funcaoFitness)(void *)) override;
    
    void mutacaoDelta();
    void mutacaoGaussiana();
    pair<IndividuoReal*, IndividuoReal*> crossover(IndividuoReal *i, string tipo);
    pair<IndividuoReal*,IndividuoReal*> crossoverUmPonto(IndividuoReal *i);
    pair<IndividuoReal*,IndividuoReal*> crossoverUniforme(IndividuoReal *i);
    pair<IndividuoReal*,IndividuoReal*> crossoverBLX(IndividuoReal *i);
    pair<IndividuoReal*,IndividuoReal*> crossoverAritmetico(IndividuoReal *i);
    
    double distancia(IndividuoReal* b);
private:
    vector<double> *cromossomo;
};

#endif /* INDIVIDUOREAL_H */

