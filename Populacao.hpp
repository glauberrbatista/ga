/* 
 * File:   Populacao.cpp
 * Author: glauber
 * 
 * Created on 19 de Maio de 2016, 22:19
 */
#include <iostream>
#include <list>
#include <fstream>
#include "Populacao.h"
#include "json.hpp"

using json = nlohmann::json;

template <class T>
void Populacao<T>::criarPopulacao(string arquivo) {
    auto entrada = json::parse(arquivo);

    chanceCrossover = entrada["chanceCrossover"];
    chanceMutacao = entrada["chanceMutacao"];
    tipoCrossover = entrada["tipoCrossover"];
    tipoMutacao = entrada["tipoMutacao"];
    tipoSelecao = entrada["selecao"];
    tipoFitness = entrada["tipoFitness"];
    if (tipoSelecao == "torneio") {
        torneio_k = entrada["k"];
    }
    escLinear = entrada["escLinear"];
    tamPop = entrada["tamPop"];
    numGeracoes = entrada["geracoes"];
    variaveis = new list<variavel*>();
    for (int i = 0; i < entrada["qtdVariaveis"]; i++) {
        variavel *v = (variavel*) malloc(sizeof (variavel));
        v->min = entrada["variaveis"][i]["min"];
        v->max = entrada["variaveis"][i]["max"];
        v->precisao = entrada["variaveis"][i]["precisao"];

        variaveis->insert(variaveis->end(), v);
    }
    setVariaveis(variaveis);
    elitismo = entrada["elitismo"];
    gap_g = entrada["g"];

    setQtdGenes(getVariaveis()->size());
    for (int i = 0; i < this->getTamPop(); i++) {
        this->populacao.insert(this->populacao.end(), new T(this->variaveis));
    }
    for (T *ind : populacao) {
        ind->setChanceCrossover(chanceCrossover);
        ind->setChanceMutacao(chanceMutacao);
        ind->setTipoMutacao(tipoMutacao);
    }
    crowd = entrada["crowding"];
    fCrowding = entrada["fcrowding"];
    melhor = new T(variaveis);
    melhor->setChanceCrossover(chanceCrossover);
    melhor->setChanceMutacao(chanceMutacao);
    melhor->setTipoMutacao(tipoMutacao);
    pior = new T(variaveis);
    pior->setChanceCrossover(chanceCrossover);
    pior->setChanceMutacao(chanceMutacao);
    pior->setTipoMutacao(tipoMutacao);
    melhorDeTodos = new T(variaveis);
    melhorDeTodos->setChanceCrossover(chanceCrossover);
    melhorDeTodos->setChanceMutacao(chanceMutacao);
    melhorDeTodos->setTipoMutacao(tipoMutacao);
    somatorio = new double();
}

template <class T>
void Populacao<T>::setPior(T* pior) {
    *this->pior = *pior;
}

template <class T>
T* Populacao<T>::getPior() {
    return pior;
}

template <class T>
void Populacao<T>::setPopulacao(list<T*> populacao) {
    this->populacao = populacao;
}

template <class T>
list<T*> Populacao<T>::getPopulacao() {
    return populacao;
}

template <class T>
void Populacao<T>::inserirPopulacao(T individuo) {

}

template <class T>
void Populacao<T>::setSomatorio(double somatorio) {
    *this->somatorio = somatorio;
}

template <class T>
double Populacao<T>::getSomatorio() {
    return *somatorio;
}

template <class T>
void Populacao<T>::setChanceCrossover(double chanceCrossover) {
    this->chanceCrossover = chanceCrossover;
}

template <class T>
double Populacao<T>::getChanceCrossover() {
    return chanceCrossover;
}

template <class T>
void Populacao<T>::setChanceMutacao(double chanceMutacao) {
    this->chanceMutacao = chanceMutacao;
}

template <class T>
double Populacao<T>::getChanceMutacao() {
    return chanceMutacao;
}

template <class T>
void Populacao<T>::setTamPop(int tamPop) {
    this->tamPop = tamPop;
}

template <class T>
int Populacao<T>::getTamPop() {
    return tamPop;
}

template <class T>
void Populacao<T>::setTamCromossomo(int tamCromossomo) {
    this->tamCromossomo = tamCromossomo;
}

template <class T>
int Populacao<T>::getTamCromossomo() {
    return tamCromossomo;
}

template <class T>
void Populacao<T>::setQtdGenes(int qtdGenes) {
    this->qtdGenes = qtdGenes;
}

template <class T>
int Populacao<T>::getQtdGenes() {
    return qtdGenes;
}

template <class T>
void Populacao<T>::setMelhorDeTodos(T* melhorDeTodos) {
    *this->melhorDeTodos = *melhorDeTodos;
}

template <class T>
T* Populacao<T>::getMelhorDeTodos() {
    return melhorDeTodos;
}

template <class T>
void Populacao<T>::setMelhor(T* melhor) {
    *this->melhor = *melhor;
}

template <class T>
T* Populacao<T>::getMelhor() {
    return melhor;
}

template <class T>
void Populacao<T>::setVariaveis(list<variavel*>* variaveis) {
    this->variaveis = variaveis;
}

template <class T>
list<variavel*>* Populacao<T>::getVariaveis() {
    return variaveis;
}

template <class T>
void Populacao<T>::mostrar() {

    for (T *i : getPopulacao()) {
        cout << "+----------------------------------------------------------+" << endl;
        cout << "| Cromossomo:\t|";
        i->mostrarIndividuo();
        cout << "| Fitness:\t" << i->getFitness() << endl;
        cout << "| Objetivo:\t" << i->getObjetivo() << endl;
        cout << "+----------------------------------------------------------+" << endl;
    }

}

template <class T>
void Populacao<T>::fitness(pair<double, double> (*funcaoFitness)(void *)) {
    static double melhor, pior;
    double fit = 0.0;
    setSomatorio(0.0);

    /*Inicializa o melhor o e pior fitness para comparação*/
    melhor = getMelhor()->getFitness();
    pior = getPopulacao().front()->getFitness();

    for (T *i : this->getPopulacao()) {
        i->calcFitness(funcaoFitness);
        fit = i->getFitness();
        setSomatorio(getSomatorio() + fit);
        if (fit > melhor) {
            melhor = fit;
            setMelhor(i);
            if (fit > this->getMelhorDeTodos()->getFitness()) {
                setMelhorDeTodos(i);
            }
        } else if (fit < pior) {
            pior = fit;
            setPior(i);
        }
    }

    if (escLinear) {
        escalonamentoLinear();
    }
}

template <class T>
void Populacao<T>::mutacao() {
    for (T *ind : getPopulacao()) {
        ind->mutacao();
    }
}

template <class T>
list<pair<T, T>>*Populacao<T>::selecao(int gap) {
    list<pair<T, T>> *listaCrossover = new list<pair<T, T >> ();
    list<T*> aux;
    pair<T, T> auxInd;

    if (tipoSelecao == "torneio") {
        for (int j = 0; j < (int) (gap / 2); j++) {
            aux = this->getPopulacao();
            auxInd.first = *torneio(&aux);
            auxInd.second = *torneio(&aux);
            listaCrossover->insert(listaCrossover->end(), auxInd);
        }
    } else if (tipoSelecao == "roleta") {
        for (int i = 0; i < (int) (gap / 2); i++) {
            aux = this->getPopulacao();
            auxInd.first = *roleta(&aux);
            auxInd.second = *roleta(&aux);
            listaCrossover->insert(listaCrossover->end(), auxInd);
        }
    }
    return listaCrossover;
}

template <class T>
void Populacao<T>::evoluir(int geracao) {
    list<pair<T, T>>*listaCrossover;
    list<T*> pTemp;
    geracaoAtual = geracao;
    int gap;
    //if (geracaoAtual / numGeracoes < 0.8) {
        gap = generationGap();
//    } else {
//        gap = getTamPop() / 2;
//    }

    if (gap % 2 != 0)
        gap--;

    listaCrossover = selecao(gap);

    Populacao popIntermediaria = geraPopIntermediaria();
    popIntermediaria.setPopulacao(*crossover(listaCrossover));
    copy_n(getPopulacao().begin(), getTamPop() - gap, back_inserter(pTemp));
    popIntermediaria.anexarPopulacao(pTemp);
    if(crowd){
        popIntermediaria.setPopulacao(crowding(popIntermediaria.getPopulacao()));
    }
    *this = popIntermediaria;

    mutacao();
    if (elitismo) {
        aplicaElitismo();
    }
}

template <class T>
T* Populacao<T>::roleta(list<T*>* individuos) {
    static mt19937 gerador(time(NULL));
    uniform_real_distribution<double> dis(0, 100);

    double sorteio = dis(gerador);
    double valorAcumulado = 0.0;

    for (T *ind : *individuos) {
        valorAcumulado += ((double) ind->getFitness() / getSomatorio())*100;
        if (sorteio < valorAcumulado) {
            individuos->remove(ind);
            return ind;
        }
    }
    //se não achar nenhum indivíduo, é o último
    return individuos->back();
}

template <class T>
T* Populacao<T>::torneio(list<T*>* individuos) {
    static mt19937 gerador(time(NULL));
    T *vencedor = new T(getVariaveis());
    int posicao, j = 0;

    for (int i = 0; i < torneio_k; i++) {
        uniform_int_distribution<int> dis(0, individuos->size());
        posicao = dis(gerador);
        typename list<T*>::iterator it;
        for (it = individuos->begin(); it != individuos->end(); it++) {
            if (posicao == j) {
                vencedor = *it;
                //individuos->erase(it);
            }
            j++;
        }
        j = 0;
    }

    return vencedor;
}

template <class T>
list<T*>* Populacao<T>::crossover(list<pair<T, T>>*listaCrossover) {
    list<T*> *individuos = new list<T*>();
    pair<T*, T*> resultado;

    for (pair<T, T> pi : *listaCrossover) {
        resultado = pi.first.crossover(&pi.second, tipoCrossover);
        individuos->insert(individuos->end(), resultado.first);
        individuos->insert(individuos->end(), resultado.second);
    }
    return individuos;
}

template <class T>
double Populacao<T>::distancia() {
    int temp = 0;
    T *a, *b;
    int l = 0;
    vector<T*> *v = new vector<T*>(this->populacao.begin(), this->populacao.end());

    for (int i = 0; i < getTamPop(); i++) {
        a = v->at(i);
        for (int j = i + 1; j < getTamPop(); j++) {
            b = v->at(j);
            temp += a->distancia(b);
            l++;
        }
    }

    return temp / l;
}

template <class T>
void Populacao<T>::aplicaElitismo() {
    T *i = new T();
    *i = *getMelhor();
    *this->populacao.back() = *i;
}

template <class T>
Populacao<T> Populacao<T>::geraPopIntermediaria() {
    Populacao<T> popIntermediaria;

    popIntermediaria = *this;
    //popIntermediaria.limparPopulacao();

    return popIntermediaria;
}

template <class T>
int Populacao<T>::generationGap() {
    return getTamPop() * gap_g / 100;
}

template <class T>
void Populacao<T>::anexarPopulacao(list<T*> l) {
    this->populacao.merge(l);
}

template <class T>
list<T*> Populacao<T>::crowding(list<T*> nova) {
    static mt19937 gerador(time(NULL));
    uniform_int_distribution<int> escolhido(0, getTamPop()-1);
    vector<T*> *nPop = new vector<T*>(nova.begin(), nova.end());
    list<T*> retorno;
    
    list<T*> *subs;
    T* ind;
    T* chosenOne;
    for(T *ind : this->getPopulacao()){
        int menorDist = 99999;
        subs = new list<T*>();
        for(int i = 0; i < fCrowding; i++){
            ind = new T(variaveis);
            *ind = *nPop->at(escolhido(gerador));
            subs->push_back(ind);
        }
        
        for(T *sub : *subs){
            if (ind->distancia(sub) < menorDist){
                menorDist = ind->distancia(sub);
                chosenOne = sub;
            }
        }
        
        retorno.push_back(chosenOne);
    }
    
    return retorno;
}

template <class T>
void Populacao<T>::escalonamentoLinear() {
    double fit_avg = (double) ((double) getSomatorio() / (double) getTamPop());
    double fit_max = getMelhor()->getFitness();
    double fit_min = getPior()->getFitness();
    double c = cEL;
    double d;
    double percentual;
    percentual = (double) ((double) geracaoAtual / (double) numGeracoes);

    if (percentual < 0.2) {
        cEL = 1.2;
    } else if (percentual > 0.2 && percentual < 0.8) {
        cEL = 1.2 + 0.8 * percentual; //arrumar isso
    } else {
        cEL = 2.0;
    }


    //cout << getSomatorio() << endl;
    if (fit_min > (c * fit_avg - fit_max) / c - 1) {
        alphaEL = ((c - 1.0) * fit_avg) / (fit_max - fit_avg);
        betaEL = fit_avg * (fit_max - (c * fit_avg)) / (fit_max - fit_avg);
    } else {
        d = fit_avg - fit_min;
        alphaEL = fit_avg / (fit_avg - fit_min);
        betaEL = -fit_min * fit_avg / (fit_avg - fit_min);
    }

    for (T* ind : getPopulacao()) {
        ind->setFitness(alphaEL * ind->getFitness() + betaEL);
        if (ind->getFitness() < 0)
            ind->setFitness(0.0);
        setSomatorio(getSomatorio() + ind->getFitness());
    }
}