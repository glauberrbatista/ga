/* 
 * File:   main.cpp
 * Author: glauber
 *
 * Created on May 30, 2016, 9:49 PM
 */

#include <cstdlib>
#include <fstream>
#include <sstream>
#include "Populacao.h"
#include "Populacao.hpp"

using namespace std;
using json = nlohmann::json;

/*
 * 
 * Os protótipos dos Fitness vão aqui
 */
void verificaFitnessBin(Populacao<IndividuoBinario> pop, string tipoFitness);
void verificaFitnessReal(Populacao<IndividuoReal> pop, string tipoFitness);
pair<double, double> fitnessMatematicoBin(void *info);
pair<double, double> fitnessBitsAlternados(void *info);
pair<double, double> fitnessRadios(void *info);
pair<double, double> fitnessF3(void *info);
pair<double, double> fitnessF3S(void *info);
pair<double, double> fitnessDN4(void *info);
/*
 * Fitness Real
 */
pair<double, double> fitnessMatematicoReal(void *info);
pair<double, double> fitnessSchaffer(void *info);
pair<double, double> fitnessAckley(void *info);
pair<double, double> fitnessRastrigin(void *info);

void verificaFitnessBin(Populacao<IndividuoBinario> pop, string tipoFitness) {
    if (tipoFitness == "bitsalternados") {
        pop.fitness(fitnessBitsAlternados);
    } else if (tipoFitness == "matematico") {
        pop.fitness(fitnessMatematicoBin);
    } else if (tipoFitness == "radios") {
        pop.fitness(fitnessRadios);
    } else if (tipoFitness == "f3") {
        pop.fitness(fitnessF3);
    } else if (tipoFitness == "f3s") {
        pop.fitness(fitnessF3S);
    } else if (tipoFitness == "dn4") {
        pop.fitness(fitnessDN4);
    } else {
        cout << "-- Fitness inválido. Implemente o fitness desejado ou corrija o nome. Saindo..." << endl;
        cout << tipoFitness << endl;
        exit(1);
    }
}

void verificaFitnessReal(Populacao<IndividuoReal> pop, string tipoFitness) {
    if (tipoFitness == "matematico") {
        pop.fitness(fitnessMatematicoReal);
    } else if (tipoFitness == "schaffer") {
        pop.fitness(fitnessSchaffer);
    } else if (tipoFitness == "ackley") {
        pop.fitness(fitnessAckley);
    } else if (tipoFitness == "rastrigin") {
        pop.fitness(fitnessRastrigin);
    } else {
        cout << "-- Fitness inválido. Implemente o fitness desejado ou corrija o nome. Saindo..." << endl;
        cout << tipoFitness << endl;
        exit(1);
    }
}

int main(int argc, char** argv) {
    if (argc != 2) {
        cout << "Argumentos inválidos. Saindo..." << endl;
        exit(1);
    }

    ifstream texto(argv[1]);
    stringstream buffer;
    buffer << texto.rdbuf();
    texto.close();
    auto entrada = json::parse(buffer.str());

    int geracoes = entrada["geracoes"];
    int execucoes = entrada["execucoes"];
    string codificacao = entrada["codificacao"];
    string tipoFitness = entrada["tipoFitness"];

    /*
     * Vetores usados para calcular as médias, e desvio padrão
     */
    double resultado[execucoes][geracoes];
    double mediamedia[execucoes][geracoes];
    double diversidade[execucoes][geracoes];
    double objetivo[execucoes][geracoes];
    double fitnessFinal, objetivoFinal;

    for (int i = 0; i < execucoes; i++) {

        //Populacao<IndividuoBinario> pop;
        /******************************
         * Verifica a Codificação
         ******************************/
        if (codificacao == "binaria") {
            Populacao<IndividuoBinario> pop;
            pop.criarPopulacao(buffer.str());

            cout << i + 1 << "ª Execução: " << endl;
            verificaFitnessBin(pop, tipoFitness);
            for (int j = 0; j < geracoes; j++) {
                //cout << j + 1 << "ª Geração:" << endl;
                pop.evoluir(j + 1);
                verificaFitnessBin(pop, tipoFitness);
                diversidade[i][j] = pop.distancia();
                resultado[i][j] = pop.getMelhor()->getFitness();
                objetivo[i][j] = pop.getMelhor()->getObjetivo();
                mediamedia[i][j] = pop.getSomatorio() / pop.getTamPop();
            }
            cout << "+------------------------------------------------------+" << endl;
            cout << "|                   Fim da " << i + 1 << "ª Execução                 |" << endl;
            cout << "+------------------------------------------------------+" << endl;
            cout << "|" << endl;
            cout << "| Melhor Fitness: " << pop.getMelhor()->getFitness() << endl;
            cout << "| Melhor Objetivo: " << pop.getMelhor()->getObjetivo() << endl;
            cout << "|" << endl;
            cout << "+-------------------------------------------------------" << endl;
            fitnessFinal += pop.getMelhor()->getFitness();
            objetivoFinal += pop.getMelhor()->getObjetivo();
        } else {
            Populacao<IndividuoReal> pop;
            pop.criarPopulacao(buffer.str());

            cout << i + 1 << "ª Execução: " << endl;
            verificaFitnessReal(pop, tipoFitness);
            for (int j = 0; j < geracoes; j++) {
                //cout << j + 1 << "ª Geração:" << endl;
                pop.evoluir(j + 1);
                verificaFitnessReal(pop, tipoFitness);
                diversidade[i][j] = pop.distancia();
                resultado[i][j] = pop.getMelhor()->getFitness();
                objetivo[i][j] = pop.getMelhor()->getObjetivo();
                mediamedia[i][j] = pop.getSomatorio() / pop.getTamPop();
            }
            cout << "+------------------------------------------------------+" << endl;
            cout << "|                   Fim da " << i + 1 << "ª Execução                 |" << endl;
            cout << "+------------------------------------------------------+" << endl;
            cout << "|" << endl;
            cout << "| Melhor Fitness: " << pop.getMelhor()->getFitness() << endl;
            cout << "| Melhor Objetivo: " << pop.getMelhor()->getObjetivo() << endl;
            cout << "|" << endl;
            cout << "+-------------------------------------------------------" << endl;
            fitnessFinal += pop.getMelhor()->getFitness();
            objetivoFinal += pop.getMelhor()->getObjetivo();
        }
    }

    FILE *fMedia = fopen("output/media.dat", "w+");
    FILE *fDesvio = fopen("output/desvio.dat", "w+");
    FILE *fMediaMedia = fopen("output/mediaDasMedias.dat", "w+");
    FILE *fDiversidade = fopen("output/diversidade.dat", "w+");
    FILE *fObjetivo = fopen("output/objetivo.dat", "w+");
    double media;
    double mediaObjetivo;
    double desvio;
    double mediaM;
    double div;
    for (int j = 0; j < geracoes; j++) {
        media = 0.0;
        desvio = 0.0;
        mediaM = 0.0;
        div = 0.0;
        mediaObjetivo = 0.0;
        for (int i = 0; i < execucoes; i++) {
            mediaM += (double) mediamedia[i][j];
            media += (double) resultado[i][j];
            mediaObjetivo += (double) objetivo[i][j];
        }
        //Média
        media = (double) (media / (double) execucoes);
        fprintf(fMedia, "%d %lf\n", j, media);
        mediaM = (double) (mediaM / (double) execucoes);
        fprintf(fMediaMedia, "%d %lf\n", j, mediaM);
        mediaObjetivo = (double) (mediaObjetivo / (double) execucoes);
        fprintf(fObjetivo, "%d %lf\n", j, mediaObjetivo);

        //Desvio padrão
        for (int i = 0; i < execucoes; i++) {
            desvio += (double) pow((resultado[i][j] - media), 2.0);
        }

        desvio = (double) (sqrt((1 / ((double) execucoes - 1)) * desvio));
        fprintf(fDesvio, "%d %lf\n", j, desvio);

        for (int i = 0; i < execucoes; i++) {
            div += diversidade[i][j];
        }
        div = div / execucoes;
        fprintf(fDiversidade, "%d %lf\n", j, div);

    }
    cout << "\n\n" << endl;
    cout << "+------------------------------------------------------+" << endl;
    cout << "|                   Fim das Execuções                  |" << endl;
    cout << "+------------------------------------------------------+" << endl;
    cout << "|" << endl;
    cout << "| Média dos Fitness Finais: " << fitnessFinal / execucoes << endl;
    cout << "| Média dos Objetivos Finais: " << objetivoFinal / execucoes << endl;
    cout << "| Desvio Padrão Final: " << desvio << endl;
    cout << "| Variância: " << endl;
    cout << "|" << endl;
    cout << "+------------------------------------------------------" << endl;
    fclose(fMedia);
    fclose(fDesvio);
    fclose(fMediaMedia);
    fclose(fDiversidade);
    fclose(fObjetivo);

    return 0;
}

/* Aqui vão as funções de Fitness*/

// -- Fitness Matemático - Função Cosseno (Real)

pair<double, double> fitnessMatematicoReal(void *info) {
    list<double> *genes = (list<double>*) info;
    pair<double, double> retorno;
    double x = genes->front();

    double obj = cos(20 * x) - abs(x) / 2 + pow(x, 3.0) / 4;
    cout << obj << "-" << obj + 4.0 << endl;
    retorno.first = obj;
    retorno.second = obj + 4.0;

    return retorno;
}

// -- Fitness Matemático - Função Cosseno (Binário)

pair<double, double> fitnessMatematicoBin(void *info) {
    list<pair<double, string>> *genes = (list<pair<double, string>>*) info;
    pair<double, double> retorno;
    double x = genes->front().first;

    double obj = cos(20 * x) - abs(x) / 2 + pow(x, 3.0) / 4;
    cout << obj << "-" << obj + 4.0 << endl;
    retorno.first = obj;
    retorno.second = obj + 4.0;

    return retorno;
}

pair<double, double> fitnessBitsAlternados(void *info) {
    list<pair<double, string>> *genes = (list<pair<double, string>>*) info;
    string cromossomo;
    pair<double, double> retorno;

    for (pair<double, string> p : *genes) {
        cromossomo += p.second;
    }
    int fitness = 0;
    char ant = cromossomo[0];
    char atual;
    for (int i = 1; i < cromossomo.length(); i++) {
        atual = cromossomo[i];
        if (atual != ant) {
            fitness++;
        }
        ant = atual;
    }
    retorno.first = retorno.second = (double) fitness;

    return retorno;
}

// -- Fitness Radios

pair<double, double> fitnessRadios(void *info) {
    list<pair<double, string>> *genes = (list<pair<double, string>>*) info;
    //list<double> *genes = (list<double>*) info;
    pair<double, double> retorno;
    double penalidade = 0.0;
    double r = -1.0; //coeficiente penalidade
    double quantidade = genes->front().first + 2 * genes->back().first;

    //double objetivo = funcaoObjetivo(genes);
    //cout << genes->front() << "-" << genes->back() << endl;
    double objetivo = genes->front().first * 30 + genes->back().first * 40;
    retorno.first = objetivo;

    if (quantidade < 40)
        penalidade = 0;
    else
        penalidade = (double) (quantidade - 40) / 16.0;

    double fit = objetivo / 1360 + (r * penalidade);
    retorno.second = fit * 1360;

    return retorno;
}

//Fitness F3 - Deceptive (Binário)

pair<double, double> fitnessF3(void *info) {
    list<pair<double, string>> *genes = (list<pair<double, string>>*) info;
    pair<double, double> retorno;
    double objetivo = 0;

    for (pair<double, string> gene : *genes) {
        if (gene.second == "000") {
            objetivo += 28;
        } else if (gene.second == "001") {
            objetivo += 26;
        } else if (gene.second == "010") {
            objetivo += 22;
        } else if (gene.second == "011") {
            objetivo += 0;
        } else if (gene.second == "100") {
            objetivo += 14;
        } else if (gene.second == "101") {
            objetivo += 0;
        } else if (gene.second == "110") {
            objetivo += 0;
        } else if (gene.second == "111") {
            objetivo += 30;
        }
    }
    //cout << objetivo << endl;
    retorno.first = objetivo;
    retorno.second = objetivo;
    return retorno;
}

pair<double, double> fitnessF3S(void *info) {
    list<pair<double, string>> *genes = (list<pair<double, string>>*) info;
    string cromossomo;
    for (pair<double, string> gene : *genes) {
        cromossomo += gene.second;
    }
    pair<double, double> retorno;
    double objetivo = 0;
    string tripla;

    for (int i = 0; i < cromossomo.length() - 20; i++) {
        tripla = cromossomo.at(i);
        tripla += cromossomo.at(i + 10);
        tripla += cromossomo.at(i + 20);
        if (tripla == "000") {
            objetivo += 28;
        } else if (tripla == "001") {
            objetivo += 26;
        } else if (tripla == "010") {
            objetivo += 22;
        } else if (tripla == "011") {
            objetivo += 0;
        } else if (tripla == "100") {
            objetivo += 14;
        } else if (tripla == "101") {
            objetivo += 0;
        } else if (tripla == "110") {
            objetivo += 0;
        } else if (tripla == "111") {
            objetivo += 30;
        }
    }

    retorno.first = objetivo;
    retorno.second = objetivo;
    return retorno;
}

pair<double, double> fitnessDN4(void *info) {
    list<pair<double, string>> *genes = (list<pair<double, string>>*) info;
    pair<double, double> retorno;
    double objetivo = 0;

    for (pair<double, string> gene : *genes) {
        int count = 0;
        for (int i = 0; i < gene.second.length(); i++) {
            if (gene.second.at(i) == '1') {
                count++;
            }
        }

        if (count == 0) {
            objetivo += genes->size() + 1;
        } else {
            objetivo += count;
        }
        //cout << objetivo << endl;
    }
    //cout << objetivo << endl;
    retorno.first = objetivo;
    retorno.second = objetivo;
    return retorno;
}

pair<double, double> fitnessSchaffer(void *info) {
    vector<double> *cromossomo = (vector<double>*) info;
    pair<double, double> retorno;
    double obj = 0.0;

    for (int i = 0; i < cromossomo->size() - 1; i++) {
        obj += pow(pow(cromossomo->at(i), 2) + pow(cromossomo->at(i + 1), 2), 0.25) * (pow(sin(50 * pow(pow(cromossomo->at(i), 2) + pow(cromossomo->at(i + 1), 2), 0.1)), 2) + 1.0);
    }

    retorno.first = obj;
    retorno.second = 50 - obj;

    return retorno;
}

pair<double, double> fitnessRastrigin(void *info) {
    vector<double> *cromossomo = (vector<double>*) info;
    pair<double, double> retorno;
    double obj = 10 * cromossomo->size();

    for (double gene : *cromossomo) {
        obj += pow(gene, 2) - (10 * cos(2 * M_PI * gene));
    }

    obj = 150 - obj;
    retorno.first = obj;
    retorno.second = obj;

    return retorno;
}

pair<double, double> fitnessAckley(void *info) {
    vector<double> *cromossomo = (vector<double>*) info;
    pair<double, double> retorno;
    double obj = 0.0;
    double firstSum = 0.0, secondSum = 0.0;

    for (double gene : *cromossomo) {
        firstSum += pow(gene, 2);
        secondSum += cos(2 * M_PI * gene);
    }

    double n = (double) cromossomo->size();

    obj = 50 - (-20 * exp(-0.2 * sqrt(firstSum / n)) - exp(secondSum / n) + 20 + exp(1));
    retorno.first = obj;
    retorno.second = obj;

    return retorno;
}