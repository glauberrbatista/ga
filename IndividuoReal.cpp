/* 
 * File:   IndividuoReal.cpp
 * Author: glauber
 * 
 * Created on 22 de Abril de 2016, 14:49
 */

#include "IndividuoReal.h"
#include "Populacao.hpp"

IndividuoReal::IndividuoReal() {
    this->setFitness(0.0);
    this->setObjetivo(0.0);
}

IndividuoReal::IndividuoReal(list<variavel*> *variaveis) {
    setVariaveis(variaveis);
    setQtdGenes(variaveis->size());
    cromossomo = new vector<double>();
    gerarIndividuo();
}

IndividuoReal::IndividuoReal(const IndividuoReal& orig) {
    *this = orig;
}

IndividuoReal::~IndividuoReal() {
}

void IndividuoReal::setCromossomo(vector<double>* cromossomo) {
    this->cromossomo = cromossomo;
}

vector<double>* IndividuoReal::getCromossomo() {
    return cromossomo;
}

void IndividuoReal::mostrarIndividuo() {
    for (double gene : *getCromossomo()) {
        cout << gene << "|";
    }
    cout << endl;
}

void IndividuoReal::gerarIndividuo() {
    static mt19937 gerador(time(NULL));
    for (variavel *v : *getVariaveis()) {
        uniform_real_distribution<double> ran(v->min, v->max);
        cromossomo->push_back(round((ran(gerador) / v->precisao)) * v->precisao);
    }

    this->setFitness(0.0);
    this->setObjetivo(0.0);
}

void IndividuoReal::mutacao() {
    //if (getTipoMutacao() == "delta") {
        mutacaoDelta();
//    } else if (getTipoMutacao() == "gaussiana") {
//        mutacaoGaussiana();
//    } else {
//        cout << "- Tipo de mutação inválido" << endl;
//        exit(1);
//    }
}

void IndividuoReal::mutacaoDelta() {
    static mt19937 gerador(time(NULL));
    list<variavel*> var = *getVariaveis();

    uniform_real_distribution<double> prob(0, 100);
    double ran;
    double gene;
    int i = 0;
    for (variavel* var : *getVariaveis()) {
        ran = prob(gerador);
        uniform_real_distribution<double> delta(var->min, var->max);
        if (ran < getChanceMutacao()) {
            gene = cromossomo->at(i);
            gene += delta(gerador) / 10;
            cromossomo->at(i) = (int) (gene / var->precisao) * var->precisao;
        }
        i++;
    }
}

void IndividuoReal::mutacaoGaussiana() {
    static mt19937 gerador(time(NULL));
    list<variavel*> var = *getVariaveis();
    uniform_real_distribution<double> prob(0, 100);
    double ran;
    double gene;
    int i = 0;
    for (variavel* var : *getVariaveis()) {
        ran = prob(gerador);
        normal_distribution<double> gauss(var->min, var->max);
        if (ran < getChanceMutacao()) {
            gene = cromossomo->at(i);
            gene += gauss(gerador);
            cromossomo->at(i) = (int) (gene / var->precisao) * var->precisao;
        }
    }

}

void IndividuoReal::calcFitness(pair<double, double> (*funcaoFitness)(void *)) {
    //list<double> genes(cromossomo->begin(), cromossomo->end());
    //pair<double, double> resultado = funcaoFitness(&genes);
    pair<double, double> resultado = funcaoFitness(getCromossomo());

    setObjetivo(resultado.first);
    setFitness(max(0.0, resultado.second));
}

pair<IndividuoReal*, IndividuoReal*> IndividuoReal::crossover(IndividuoReal *i, string tipo) {
    if (tipo == "umcorte") {
        return crossoverUmPonto(i);
    } else if (tipo == "uniforme") {
        return crossoverUniforme(i);
    } else if (tipo == "blx") {
        return crossoverBLX(i);
    } else if (tipo == "aritmetico") {
        return crossoverAritmetico(i);
    } else {
        cout << "- Tipo de Crossover Inválido" << endl;
        exit(1);
    }
}

pair<IndividuoReal*, IndividuoReal*> IndividuoReal::crossoverUmPonto(IndividuoReal *i) {
    pair<IndividuoReal*, IndividuoReal*> resultado;
    resultado.first = new IndividuoReal();
    resultado.second = new IndividuoReal();
    *resultado.first = *this;
    *resultado.second = *i;

    static mt19937 gerador(time(NULL));
    uniform_int_distribution<int> dis(1, getCromossomo()->size() - 1);
    uniform_real_distribution<double> dis_double(0, 100);
    vector<double> *cromo1, *cromo2;
    cromo1 = new vector<double>();
    cromo2 = new vector<double>();

    if (dis_double(gerador) <= getChanceCrossover()) {
        int corte = dis(gerador);

        for (int k = 0; k < corte; k++) {
            cromo1->push_back(this->getCromossomo()->at(k));
            cromo2->push_back(i->getCromossomo()->at(k));
        }
        for (int k = corte; k < getQtdGenes(); k++) {
            cromo1->push_back(i->getCromossomo()->at(k));
            cromo2->push_back(this->getCromossomo()->at(k));
        }

        resultado.first->setCromossomo(cromo1);
        resultado.second->setCromossomo(cromo2);
    }

    return resultado;
}

pair<IndividuoReal*, IndividuoReal*> IndividuoReal::crossoverUniforme(IndividuoReal *i) {
    pair<IndividuoReal*, IndividuoReal*> resultado;
    resultado.first = new IndividuoReal();
    resultado.second = new IndividuoReal();
    *resultado.first = *this;
    *resultado.second = *i;

    vector<double> *cromo1, *cromo2;
    cromo1 = new vector<double>();
    cromo2 = new vector<double>();
    *cromo1 = *this->getCromossomo();
    *cromo2 = *i->getCromossomo();
    string mascara = stringBinaria(this->getCromossomo()->size());
    double troca;

    for (int k = 0; k < mascara.length(); k++) {
        if (mascara[k] == '1') {
            troca = cromo1->at(k);
            cromo1->at(k) = cromo2->at(k);
            cromo2->at(k) = troca;
        }
        resultado.first->setCromossomo(cromo1);
        resultado.second->setCromossomo(cromo2);
    }

    return resultado;
}

pair<IndividuoReal*, IndividuoReal*> IndividuoReal::crossoverBLX(IndividuoReal *i) {
    pair<IndividuoReal*, IndividuoReal*> resultado;
    resultado.first = new IndividuoReal();
    resultado.second = new IndividuoReal();
    *resultado.first = *this;
    *resultado.second = *i;

    static mt19937 gerador(time(NULL));
    uniform_real_distribution<double> coef(0, 1);
    uniform_real_distribution<double> chance(0, 100);
    double a, d;

    vector<double> *cromo1, *cromo2;
    cromo1 = new vector<double>();
    cromo2 = new vector<double>();

    if (chance(gerador) < getChanceCrossover()) {
        a = 0.5;
        //a = coef(gerador);
        for (int k = 0; k < getCromossomo()->size(); k++) {
            d = abs(this->getCromossomo()->at(k) - i->getCromossomo()->at(k));
            uniform_real_distribution<double> u(min(this->getCromossomo()->at(k), i->getCromossomo()->at(k))-(a * d),
                    max(this->getCromossomo()->at(k), i->getCromossomo()->at(k))+(a * d));
            cromo1->push_back(u(gerador));
            cromo2->push_back(u(gerador));
        }
        resultado.first->setCromossomo(cromo1);
        resultado.second->setCromossomo(cromo2);
    }

    return resultado;
}

pair<IndividuoReal*, IndividuoReal*> IndividuoReal::crossoverAritmetico(IndividuoReal *i) {
    pair<IndividuoReal*, IndividuoReal*> resultado;
    resultado.first = new IndividuoReal();
    resultado.second = new IndividuoReal();
    *resultado.first = *this;
    *resultado.second = *i;

    static mt19937 gerador(time(NULL));
    uniform_real_distribution<double> coef(0, 1);
    uniform_real_distribution<double> chance(0, 100);

    vector<double> *cromo1, *cromo2;
    cromo1 = new vector<double>();
    cromo2 = new vector<double>();

    if (chance(gerador) < getChanceCrossover()) {
        double a = coef(gerador);
        for (int k = 0; k < this->getCromossomo()->size(); k++) {
            cromo1->push_back(a * this->getCromossomo()->at(k) + (1 - a) * i->getCromossomo()->at(k));
            cromo2->push_back((1 - a) * this->getCromossomo()->at(k) + a * i->getCromossomo()->at(k));
        }
        resultado.first->setCromossomo(cromo1);
        resultado.second->setCromossomo(cromo2);
    }

    return resultado;
}

double IndividuoReal::distancia(IndividuoReal* b) {
    double soma = 0.0;
    for (int k = 0; k < getQtdGenes(); k++) {
        soma += pow(this->getCromossomo()->at(k) + b->getCromossomo()->at(k), 2.0);
    }
    return sqrt(soma);
}