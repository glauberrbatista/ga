#!/bin/sh 
name=$1
/usr/bin/gnuplot << EOF

set terminal pdf
#set terminal pngcairo size 800,600 enhanced font 'Verdana,9'

set style line 11 lc rgb '#808080' lt 1
set border 3 back ls 11
set tics nomirror
set style line 12 lc rgb '#808080' lt 0 lw 1
set grid back ls 12

set style line 1 lc rgb '#8b1a0e' pt 1 ps 1 lt 1 lw 1 # --- red
set style line 2 lc rgb '#5e9c36' pt 2 ps 1 lt 1 lw 1 # --- green

set yrange [0 : *]
set xtics rotate

set output "output/conv_$name.pdf"
set xlabel 'Gerações'
set ylabel 'Fitness'
set title "Convergência"
set key bottom right
plot "output/media.dat" title "Convergência" with lines, "output/mediaDasMedias.dat" title "Média da População" with lines, "output/objetivo.dat" title "Função Objetivo" with lines

set key top right
set output "output/div_$name.pdf"
set ylabel "Diversidade"
set title "Diversidade"
#unset yrange
set yrange [0 : *]
plot "output/diversidade.dat" title "Diversidade Populacional" with lines
