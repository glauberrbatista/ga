/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   IndividuoBinario.h
 * Author: glauber
 *
 * Created on 20 de Abril de 2016, 20:36
 */

#ifndef INDIVIDUOBINARIO_H
#define INDIVIDUOBINARIO_H

#include "Individuo.h"

class IndividuoBinario : public Individuo{
public:
    IndividuoBinario();
    IndividuoBinario(list<variavel*> *variaveis);
    IndividuoBinario(const IndividuoBinario& orig);
    virtual ~IndividuoBinario();
    void setCromossomo(string cromossomo);
    string getCromossomo();
    void setTamCromossomo(int tamCromossomo);
    int getTamCromossomo();
    void mostrarIndividuo();
    
    void gerarIndividuo() override;
    void mutacao() override;
    void calcFitness(pair<double,double> (*funcaoFitness)(void *)) override;
    pair<IndividuoBinario*,IndividuoBinario*> crossover(IndividuoBinario *i, string tipo);
    pair<IndividuoBinario*,IndividuoBinario*> crossoverUmPonto(IndividuoBinario *i);
    pair<IndividuoBinario*,IndividuoBinario*> crossoverUniforme(IndividuoBinario *i);
    double distancia(IndividuoBinario* b);
    
private:
    int tamCromossomo;
    string cromossomo;
};

#endif /* INDIVIDUOBINARIO_H */

