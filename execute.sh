#!/bin/sh


for f in `ls -1 inputs/binario/f3s-*`
do 
	echo $f >> r-$f.txt
	./algoritmo-genetico $f >> r-$f.txt &&
	./output/plot.sh $f
done

for f in `ls -1 inputs/binario/f3-*`
do 
	echo $f >> r-$f.txt
	./algoritmo-genetico $f >> r-$f.txt &&
	./output/plot.sh $f
done

for f in `ls -1 inputs/binario/dn4-*`
do 
	echo $f >> r-$f.txt
	./algoritmo-genetico $f >> r-$f.txt &&
	./output/plot.sh $f
done

for f in `ls -1 inputs/real/schaffer-*`
do 
	echo $f >> r-$f.txt
	./algoritmo-genetico $f >> r-$f.txt &&
	./output/plot.sh $f
done

for f in `ls -1 inputs/real/ackley-*`
do 
	echo $f >> r-$f.txt
	./algoritmo-genetico $f >> r-$f.txt &&
	./output/plot.sh $f
done

for f in `ls -1 inputs/real/rastrigin-*`
do 
	echo $f >> r-$f.txt
	./algoritmo-genetico $f >> r-$f.txt &&
	./output/plot.sh $f
done
