/* 
 * File:   Individuo.h
 * Author: glauber
 *
 * Created on 20 de Abril de 2016, 19:46
 */

#ifndef INDIVIDUO_H
#define INDIVIDUO_H

#include <string>
#include <random>
#include <iostream>
#include <list>

enum codificacao {binario, real, combinatorio};

typedef struct __variavel {
    double min;
    double max;
    double precisao;
} variavel;

typedef struct __info_pop {
    int tamPop;
    double mutacao;
    double crossover;
    codificacao cod;
} info_pop;

using namespace std;

class Individuo {
public:
    Individuo();
    Individuo(list<variavel*> *variaveis);
    Individuo(const Individuo& orig);
    virtual ~Individuo();
    void setVariaveis(list<variavel*>* variaveis);
    list<variavel*>* getVariaveis();
    void setObjetivo(double objetivo);
    double getObjetivo();
    void setFitness(double fitness);
    double getFitness();
    void setQtdGenes(int qtdGenes);
    int getQtdGenes();
    void setChanceMutacao(double chanceMutacao);
    double getChanceMutacao();
    void setChanceCrossover(double chanceCrossover);
    double getChanceCrossover();
    void setTipoMutacao(string tipoMutacao);
    string getTipoMutacao();

    virtual void gerarIndividuo() = 0;
    virtual void mutacao() = 0;
    virtual void calcFitness(pair<double,double> (*funcaoFitness)(void *)) = 0;
private:
    double chanceCrossover;
    double chanceMutacao;
    int qtdGenes;
    double fitness;
    double objetivo;
    list<variavel*> *variaveis;
    string tipoMutacao;
protected:
    string stringBinaria(int tamanho);
};

#endif /* INDIVIDUO_H */

