/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Populacao.h
 * Author: glauber
 *
 * Created on 19 de Maio de 2016, 22:19
 */

#ifndef POPULACAO_H
#define POPULACAO_H
#include "Individuo.h"
#include "IndividuoBinario.h"
#include "IndividuoReal.h"
#include "json.hpp"
#include <random>

template <class T>
class Populacao {
public:
    void criarPopulacao(string arquivo);
    void setPopulacao(list<T*> populacao);
    list<T*> getPopulacao();
    void inserirPopulacao(T individuo);
    void limparPopulacao();
    void anexarPopulacao();
    void setSomatorio(double somatorio);
    double getSomatorio();
    void setChanceCrossover(double chanceCrossover);
    double getChanceCrossover();
    void setChanceMutacao(double chanceMutacao);
    double getChanceMutacao();
    void setTamPop(int tamPop);
    int getTamPop();
    void setTamCromossomo(int tamCromossomo);
    int getTamCromossomo();
    void setQtdGenes(int qtdGenes);
    int getQtdGenes();
    void setMelhorDeTodos(T* melhorDeTodos);
    T* getMelhorDeTodos();
    void setMelhor(T* melhor);
    T* getMelhor();
    void setPior(T* pior);
    T* getPior();
    void setVariaveis(list<variavel*>* variaveis);
    list<variavel*>* getVariaveis();
    void mostrar();
    void anexarPopulacao(list<T*> l);

    /*Operadores, fitness e seleção*/
    void fitness(pair<double, double> (*funcaoFitness)(void *));
    list<pair<T, T>>* selecao(int gap);
    void evoluir(int geracao);
    T* roleta(list<T*>* individuos);
    T* torneio(list<T*>* individuos);
    void mutacao();
    list<T*>* crossover(list<pair<T, T>>*listaCrossover);
    void aplicaElitismo();
    
    /*Distância*/
    double distancia();

    /*Funções de controle de diversidade*/
    Populacao geraPopIntermediaria();
    int generationGap();
    void escalonamentoLinear();
    void fitnessSharing();
    list<T*> crowding(list<T*> nova);

private:
    list<T*> populacao;
    list<variavel*> *variaveis;
    T* pior;
    T* melhor;
    T* melhorDeTodos;
    int qtdGenes;
    int tamCromossomo;
    int tamPop;
    double chanceMutacao;
    double chanceCrossover;
    double* somatorio;
    string tipoSelecao;
    int torneio_k;
    bool escLinear;
    bool crowd;
    int fCrowding;
    double cEL;
    double alphaEL;
    double betaEL;
    int geracaoAtual;
    string tipoCrossover;
    string tipoMutacao;
    string tipoFitness;
    bool elitismo;
    int gap_g;
    int numGeracoes;
};
#include "Populacao.h"
#endif /* POPULACAO_H */

